# BlackJack BPMN

Attempt to put as much logic as possible into the BPMN file.
Combination of multiple rules possible even thouth I'm not sure if some of the combinations make sense.
I'm also not sure if all rules are implemented correctly because the rules vary between different sources (i.e. early surrender, resplitting, split different 10's (even though it seems you should never split 10's)). 

![BlackJackTable](./src/main/documentation/screenshots/BlackJackTable.png)

![BPMN](./src/main/documentation/screenshots/bpmn.jpg)

package es.rick.bpmn.blackjack.controller

import es.rick.bpmn.blackjack.controller.process.ProcessController
import es.rick.bpmn.blackjack.exception.BlackJackException
import es.rick.bpmn.blackjack.model.*
import org.camunda.bpm.engine.impl.context.Context
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class TableController(
    @Autowired val dealerController: DealerController,
    @Autowired val processController: ProcessController
) {
    private val BPMN_KEY = "BlackJack"

    @GetMapping("tables")
    fun getTables(): List<BlackJackProcessModel> {
        return processController.getModelForAllInstances()
    }

    @GetMapping("table/{tableId}/")
    fun getTable(@PathVariable("tableId") tableId: String): BlackJackProcessModel {
        return processController.getModelByTableId(tableId)
    }

    @PostMapping("table/{tableId}/edit-player-and-execute/")
    fun setEditPlayerAndExecuteOption(
        @PathVariable("tableId") tableId: String,
        @RequestBody player: Player,
        @RequestParam("option") option: String
    ): BlackJackProcessModel {
        var model = processController.getModelByTableId(tableId)
        model.editPlayer = player
        model.optionSelected = option
        processController.setModelByTableId(tableId, model)
        processController.signalByBusinessKey(tableId)
        model = processController.getModelByTableId(tableId)
        return model
    }

    @PostMapping("table/create")
    fun createTable(@RequestBody tableRules: TableRules): BlackJackProcessModel {
        val dealer = dealerController.findAvailableDealer()
        val table = Table(UUID.randomUUID().toString(), tableRules, dealer, mutableListOf())
        processController.startByProcessDefinitionKey(
            BPMN_KEY,
            BlackJackProcessModel(table, null, null, null, listOf(), null)
        )
        val model = processController.getModelByTableId(table.tableId)
        return model
    }

    @PostMapping("table/{tableId}/execute/")
    fun executeOption(
        @PathVariable("tableId") tableId: String,
        @RequestParam("option") option: String
    ): BlackJackProcessModel {
        val model = processController.getModelByTableId(tableId)
        model.optionSelected = option
        processController.setModelByTableId(tableId, model)
        processController.signalByBusinessKey(tableId)
        return processController.getModelByTableId(tableId)
    }
}

@RestController
class DealerController {
    @GetMapping("dealer/available/")
    fun findAvailableDealer(): Dealer {
        val firstNames = Arrays.asList("James", "Jenkins", "Alfred", "Niles", "Geoffrey")
        val lastNames = Arrays.asList("Addams", "Butler", "Pennyworth", "Smith", "Jones", "Davies")
        val randomFirstNameIndex = ((Math.random() * 1000) % firstNames.size).toInt()
        val randomLastNameIndex = ((Math.random() * 1000) % lastNames.size).toInt()
        val firstName = firstNames[randomFirstNameIndex]
        val lastName = lastNames[randomLastNameIndex]
        val name = "$firstName $lastName"
        val dealerId = UUID.randomUUID().toString()
        return Dealer(
            dealerId,
            name,
            DealerBox(Hand(UUID.randomUUID().toString(), mutableListOf(), HandStatus.Waiting), DealerBoxStatus.Inactive)
        )
    }
}

@RestController
@RequestMapping("cards")
class CardShuffler {
    @GetMapping("shuffle")
    fun shuffle(@RequestParam("decks") decks: Int = 4): Cards {
        val cards = createCards(decks)
        cards.shuffle()
        return Cards(cards)
    }

    @GetMapping("create")
    private fun createCards(@RequestParam("decks") decks: Int = 4): ArrayList<Card> {
        val cards = ArrayList<Card>()
        for (deck in 1..decks) {
            Color.values().forEach { color ->
                CardValue.values().forEach { cardValue ->
                    cards.add(Card(color, cardValue))
                }
            }
        }
        return cards
    }
}
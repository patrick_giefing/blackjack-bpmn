package es.rick.bpmn.blackjack.controller.process

import es.rick.bpmn.blackjack.model.BlackJackProcessModel

interface ProcessController {
    fun startByProcessDefinitionKey(processDefinitionKey: String, model: BlackJackProcessModel)

    fun getModelByTableId(tableId: String): BlackJackProcessModel

    fun setModelByTableId(tableId: String, model: BlackJackProcessModel)

    fun getModelForAllInstances(): List<BlackJackProcessModel>

    fun signalByBusinessKey(tableId: String)
}
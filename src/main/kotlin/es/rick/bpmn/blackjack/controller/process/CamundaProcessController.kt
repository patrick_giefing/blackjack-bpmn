package es.rick.bpmn.blackjack.controller.process

import es.rick.bpmn.blackjack.model.BlackJackProcessModel
import es.rick.bpmn.blackjack.model.ProcessVariables
import org.camunda.bpm.engine.ProcessEngine
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class CamundaProcessController(
    @Autowired val processEngine: ProcessEngine
) : ProcessController {
    override fun startByProcessDefinitionKey(processDefinitionKey: String, model: BlackJackProcessModel) {
        val m = hashMapOf<String, Any?>(ProcessVariables.model.name to model)
        processEngine.runtimeService.startProcessInstanceByKey(
            processDefinitionKey, model.table.tableId, m
        )
    }

    override fun getModelByTableId(tableId: String): BlackJackProcessModel {
        val executionId = findExecutionIdByBusinessKeyOrFail(tableId)
        return getModelByExecutionId(executionId)
    }

    fun getModelByExecutionId(executionId: String): BlackJackProcessModel {
        return processEngine.runtimeService.getVariable(
            executionId,
            ProcessVariables.model.name
        ) as BlackJackProcessModel
    }

    override fun setModelByTableId(tableId: String, model: BlackJackProcessModel) {
        val executionId = findExecutionIdByBusinessKeyOrFail(tableId)
        processEngine.runtimeService.setVariable(executionId, ProcessVariables.model.name, model)
    }

    override fun getModelForAllInstances(): List<BlackJackProcessModel> {
        val businessKeys = processEngine.runtimeService.createProcessInstanceQuery().list().map { it.businessKey }
        return businessKeys.map { getModelByTableId(it) }
    }

    fun findExecutionIdByBusinessKeyOrFail(businessKey: String): String {
        val tasks =
            processEngine.taskService.createTaskQuery().active().processInstanceBusinessKey(businessKey).list()
        if (tasks.isEmpty())
            throw Error("No active activity found for businessKey $businessKey");
        if (tasks.size === 1)
            return tasks[0].executionId
        return tasks.last().executionId
    }

    override fun signalByBusinessKey(tableId: String) {
        val executionId = findExecutionIdByBusinessKeyOrFail(tableId)
        processEngine.runtimeService.signal(executionId)
    }
}
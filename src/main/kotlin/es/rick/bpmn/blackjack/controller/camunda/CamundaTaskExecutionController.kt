package es.rick.bpmn.blackjack.controller.camunda

import es.rick.bpmn.blackjack.controller.CardShuffler
import es.rick.bpmn.blackjack.message.MessagePublisher
import es.rick.bpmn.blackjack.model.*
import org.camunda.bpm.engine.impl.context.Context
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import java.util.*

@Controller
open class CamundaTaskExecutionController(
    @Autowired val messagePublisher: MessagePublisher
) {
    fun shuffleCards() {
        val cards = CardShuffler().shuffle()
        saveCardsToContext(cards)
    }

    fun dealActivePlayerCard() {
        withModel { model ->
            withCards { cards ->
                val card = cards.drawCard()
                val player = findActivePlayerOrFail(model)
                val box = player.box
                if (box.hands.isNullOrEmpty())
                    box.hands = mutableListOf(Hand(UUID.randomUUID().toString(), mutableListOf(), HandStatus.Active))
                val hand = box.hands.last()
                hand.cards.add(card)
                model.activePlayer = player
                model.activeHand = hand
            }
        }
    }

    fun dealDealerHoleCard() {
        dealDealerCard() // TODO implement a hole card flag
    }

    fun dealDealerCard() {
        withModel { model ->
            withCards { cards ->
                val card = cards.drawCard()
                val box = model.table.dealer.box
                box.hand.cards.add(card)
            }
        }
    }

    fun showHoleCard() {
        // TODO implement hole card
    }

    fun setInsurance() {
        withModel {
            it.activePlayer?.box?.insurance = true
        }
    }

    fun payInsurance() {
        withModel {
            val player = findActivePlayerOrFail(it)
            player.balance += player.bet
        }
    }

    fun splitHand() {
        withModel { model ->
            val player = findActivePlayerOrFail(model)
            val handInd = player.box.hands.indexOfFirst { it.handId == model.activeHand?.handId }
            if (handInd < 0) return
            val hand = player.box.hands[handInd]
            if (hand.cards.size !== 2) return
            val card2 = hand.cards.removeAt(1)
            val splitHand = Hand(UUID.randomUUID().toString(), mutableListOf(card2), HandStatus.Waiting, false)
            player.box.hands.add(handInd + 1, splitHand)
            model.activePlayer = player
            model.activeHand = hand
        }
    }

    fun setBusted() {
        withModel {
            it.activeHand?.status = HandStatus.Busted
        }
    }

    fun setStaying() {
        withModel {
            it.activeHand?.status = HandStatus.Staying
        }
    }

    fun surrender() {
        withModel {
            it.activePlayer?.box?.status = PlayerBoxStatus.Surrendered
        }
    }

    fun collectInsurances() {
        withModel {
            it.table.players.forEach { player ->
                if (player.box.insurance) {
                    player.balance = player.balance - player.bet / 2
                    if (player.playerId == it.activePlayer?.playerId)
                        it.activePlayer = player
                }
            }
        }
    }

    fun collectCards() {
        withModel {
            it.table.dealer.box.hand.collectCards()
            it.table.players.forEach { player -> player.box.collectCards() }
        }
    }

    fun getInactiveHandCount(): Int {
        return withModel { model ->
            val hands = model.activePlayer?.box?.hands ?: listOf()
            val activeHandInd = hands.indexOfFirst { it.handId == model.activeHand?.handId }
            val inactiveHandCount = hands.size - activeHandInd - 1
            return inactiveHandCount
        }
    }

    private fun findActivePlayerOrFail(model: BlackJackProcessModel): Player {
        return model.table.players.find { player -> player.playerId == model.activePlayer?.playerId }
            ?: throw Error("No active player found")
    }

    fun addPlayer() {
        withModel {
            it.editPlayer ?: throw Error("Can't add player - no player set")
            it.editPlayer?.playerId = it.editPlayer?.playerId ?: UUID.randomUUID().toString()
            it.table.players.add(it.editPlayer as Player)
        }
    }

    fun checkPlayerExists() {
        withModel {
            val editPlayerId = it.editPlayer?.playerId
            editPlayerId ?: return
            val player = it.table.players.find { p -> p.playerId == editPlayerId }
            if (player === null)
                it.editPlayer = null
        }
    }

    fun removePlayer() {
        withModel {
            val editPlayerId = it.editPlayer?.playerId
            editPlayerId ?: return
            it.table.players.removeIf { p -> p.playerId == editPlayerId }
        }
    }

    fun showMessage(message: String) {
        messagePublisher.sendToAll(message)
    }

    private inline fun <R> withModel(block: (BlackJackProcessModel) -> R): R {
        val model = getModelFromContext()
        val result = block(model)
        saveModelToContext(model)
        return result
    }

    private inline fun withCards(block: (Cards) -> Unit) {
        val cards = getCardsFromContext()
        block(cards)
        saveCardsToContext(cards)
    }

    fun getModelFromContext(): BlackJackProcessModel {
        return Context.getCoreExecutionContext().execution.getVariable(ProcessVariables.model.name) as BlackJackProcessModel
    }

    fun saveModelToContext(model: BlackJackProcessModel) {
        Context.getCoreExecutionContext().execution.setVariable(ProcessVariables.model.name, model)
    }

    fun getCardsFromContext(): Cards {
        return Context.getCoreExecutionContext().execution.getVariable(ProcessVariables.cards.name) as Cards
    }

    fun saveCardsToContext(cards: Cards) {
        Context.getCoreExecutionContext().execution.setVariable(ProcessVariables.cards.name, cards)
    }
}
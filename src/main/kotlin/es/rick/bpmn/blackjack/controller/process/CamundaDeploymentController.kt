package es.rick.bpmn.blackjack.controller.process

import org.camunda.bpm.engine.ProcessEngine
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class CamundaDeploymentController(@Autowired val processEngine: ProcessEngine) : InitializingBean {
    fun deployBpmnFromClassPath(): String {
        val bpmnFileName = "BlackJack.bpmn"
        val isBpmn = CamundaDeploymentController::class.java.getResourceAsStream("/${bpmnFileName}")
        //val deployment =
        //    processEngine.repositoryService.createDeployment().addInputStream(bpmnFileName, isBpmn).deploy()
        println(
            "Available processDefinitionKeys: ${
                processEngine.repositoryService.createProcessDefinitionQuery().list().map { it -> "${it.key}:${it.id}" }
            }"
        )
        //return deployment.id
        return ""
    }

    override fun afterPropertiesSet() {
        deployBpmnFromClassPath()
    }
}
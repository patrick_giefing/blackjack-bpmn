package es.rick.bpmn.blackjack.message

interface MessagePublisher {
    fun sendToAll(message: String)
}

class ConsoleMessagePublisher : MessagePublisher {
    override fun sendToAll(message: String) {
        println(frameMessage(message))
    }

    fun frameMessage(message: String, c: Char = '*'): String {
        val x = "" + c
        val lines = message.split("\r?\n") // TODO precompile
        val maxLength = lines.map { it.length }.max() ?: 0
        val xLine = x.repeat(maxLength + 4)
        val s =
            "$xLine\r\n${lines.joinToString("\r\n") { line -> "$x $line${" ".repeat(maxLength - line.length)} $x" }}\r\n$xLine"
        return s
    }
}
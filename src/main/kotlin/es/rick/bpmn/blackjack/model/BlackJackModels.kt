package es.rick.bpmn.blackjack.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import es.rick.bpmn.blackjack.controller.DealerController
import es.rick.bpmn.blackjack.exception.BlackJackException
import java.io.Serializable

class Table @JsonCreator constructor(
    @JsonProperty("tableId") val tableId: String,
    @JsonProperty("rules") val rules: TableRules,
    @JsonProperty("dealer") val dealer: Dealer,
    @JsonProperty("players") val players: MutableList<Player>,
    @JsonProperty("gameActive") var gameActive: Boolean = false
) : Serializable {
    fun addPlayer(player: Player) {
        players.add(player)
    }

    fun removePlayer(playerId: String) {
        players.removeIf { it -> it.playerId == playerId }
    }

    override fun toString(): String {
        return "Table(tableId='$tableId', rules=$rules, dealer=$dealer, players=$players, gameActive=$gameActive)"
    }
}

class TableRules @JsonCreator constructor(
    @JsonProperty("dealerHoleCard") val dealerHoleCard: Boolean = false,
    @JsonProperty("playerCountMin") val playerCountMin: Int = 1,
    @JsonProperty("playerCountMax") val playerCountMax: Int = 7,
    @JsonProperty("earlySurrenderPossible") val earlySurrenderPossible: Boolean = false,
    @JsonProperty("lateSurrenderPossible") val lateSurrenderPossible: Boolean = false,
    @JsonProperty("insuranceAvailable") val insuranceAvailable: Boolean = false,
    @JsonProperty("maxSplits") val maxSplits: Int = 1,
    @JsonProperty("splitDifferent10s") val splitDifferent10s: Boolean = false,
    @JsonProperty("dealerStayValue") val dealerStayValue: Int = 17
) : Serializable {
    override fun toString(): String {
        return "TableRules(dealerHoleCard=$dealerHoleCard, playerCountMin=$playerCountMin, playerCountMax=$playerCountMax, earlySurrenderPossible=$earlySurrenderPossible, lateSurrenderPossible=$lateSurrenderPossible, insuranceAvailable=$insuranceAvailable, maxSplits=$maxSplits, splitDifferent10s=$splitDifferent10s, dealerStayValue=$dealerStayValue)"
    }
}

class Dealer @JsonCreator constructor(
    @JsonProperty("dealerId") val dealerId: String,
    @JsonProperty("name") val name: String,
    @JsonProperty("box") val box: DealerBox
) : Serializable {
    override fun toString(): String {
        return "Dealer(dealerId='$dealerId', name='$name', box=$box)"
    }
}

class Player @JsonCreator constructor(
    @JsonProperty("playerId") var playerId: String?,
    @JsonProperty("name") val name: String,
    @JsonProperty("playerType") val playerType: PlayerType,
    @JsonProperty("balance") var balance: Int,
    @JsonProperty("bet") var bet: Int,
    @JsonProperty("box") val box: PlayerBox = PlayerBox(mutableListOf(), PlayerBoxStatus.Inactive)
) : Serializable {
    override fun toString(): String {
        return "Player(playerId=$playerId, name='$name', playerType=$playerType, balance=$balance, bet=$bet, box=$box)"
    }
}

class PlayerBox @JsonCreator constructor(
    @JsonProperty("hands") var hands: MutableList<Hand>,
    @JsonProperty("status") var status: PlayerBoxStatus,
    @JsonProperty("insurance") var insurance: Boolean = false
) : Serializable {
    override fun toString(): String {
        return "PlayerBox(hands=$hands, status=$status)"
    }

    fun isBlackJack(): Boolean {
        if (hands.isEmpty()) return false
        return hands.first().isBlackJack()
    }

    fun canSplitLastHand(tableRules: TableRules): Boolean {
        if (hands.isEmpty()) return false
        val lastHand = hands.last()
        if (lastHand.cards.size !== 2) return false
        if (hands.size > tableRules.maxSplits) return false
        val card1 = lastHand.cards[0]
        val card2 = lastHand.cards[1]
        if (card1.value === card2.value) return true
        return card1.value.value == 10 && card2.value.value == 10 && tableRules.splitDifferent10s
    }

    fun collectCards(): List<Card> {
        val cards = mutableListOf<Card>()
        hands.forEach { hand -> cards.addAll(hand.collectCards()) }
        return cards
    }
}

class DealerBox @JsonCreator constructor(
    @JsonProperty("hand") var hand: Hand,
    @JsonProperty("status") var status: DealerBoxStatus
) : Serializable {
    override fun toString(): String {
        return "DealerBox(hand=$hand, status=$status)"
    }

    fun getVisibleCardValue(): Int {
        if (hand.cards.isEmpty()) return 0
        // TODO filter hole cards when they are implemented
        return hand.cards[0].value.value
    }

    fun collectCards(): List<Card> {
        return hand.collectCards()
    }
}

enum class PlayerType { Human, Computer }

enum class PlayerDecision { Hit, Stand, DoubleDown, Split, Surrender }

enum class DealerBoxStatus { Active, Inactive, Busted, Staying }

enum class PlayerBoxStatus { Active, Inactive, Surrendered, Busted, Staying }

enum class HandStatus { Waiting, Active, Busted, Staying }

class Hand @JsonCreator constructor(
    @JsonProperty("handId") val handId: String,
    @JsonProperty("cards") var cards: MutableList<Card>,
    @JsonProperty("status") var status: HandStatus,
    /**
     * BlackJack not possible when hand splittet
     */
    @JsonIgnore var singleHand: Boolean = true
) : Serializable {
    override fun toString(): String {
        return "Hand(cards=$cards, status=$status)"
    }

    fun isBlackJack(): Boolean {
        if (!singleHand || cards.size !== 2) return false
        val value = getHandValue()
        return value === 21
    }

    fun getHandValue(): Int {
        return getHandValues().first
    }

    fun getHandValueString(): String {
        return getHandValues().second
    }

    fun collectCards(): List<Card> {
        val cardsCopy = ArrayList(cards)
        cards.clear()
        return cardsCopy
    }

    fun setNextHandActiveForActivePlayer() {

    }

    fun getHandValues(): Pair<Int, String> {
        var value = 0
        var soft = false
        var hard = false
        cards.forEach { card ->
            var cardValue = card.value.value
            if (cardValue == 11) {
                if (value + cardValue > 21) {
                    value += 1
                } else {
                    value += cardValue
                    soft = true
                }
            } else {
                if (soft && value + cardValue > 21) {
                    value += cardValue - 10
                    soft = false
                    hard = true
                } else {
                    value += cardValue
                }
            }
        }
        val valueString =
            if (value > 21) "Busted $value" else
                (if (value === 21 && cards.size === 2) "BlackJack" else
                    (if (soft) "Soft " else if (hard) "Hard " else "") + value)
        return value to valueString
    }
}

class Card @JsonCreator constructor(
    @JsonProperty("color") val color: Color,
    @JsonProperty("value") val value: CardValue
) : Serializable {
    override fun toString(): String {
        return "Card(color=$color, value=$value)"
    }
}

enum class CardValue(val value: Int) {
    _2(2), _3(3), _4(4), _5(5), _6(6),
    _7(7), _8(8), _9(9), _10(10), J(10),
    Q(10), K(10), A(11)
}

enum class Color { Spades, Hearts, Diamonds, Clubs }

class Cards : Serializable {
    @JsonProperty("cards")
    var cards: MutableList<Card>

    @JsonCreator
    constructor(cards: List<Card>) {
        this.cards = ArrayList(cards)
    }

    fun drawCard(): Card {
        if (cards.isEmpty())
            throw BlackJackException("No cards left")
        return cards.removeAt(0)
    }
}
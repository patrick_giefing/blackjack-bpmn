package es.rick.bpmn.blackjack.model

enum class ProcessVariables {
    model, cards
}
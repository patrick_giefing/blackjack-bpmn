package es.rick.bpmn.blackjack.model

import java.io.Serializable

class BlackJackProcessModel(
    var table: Table,
    var editPlayer: Player?,
    var activePlayer: Player?,
    var activeHand: Hand?,
    var optionsAvailable: List<String>,
    var optionSelected: String?
) : Serializable {
    override fun toString(): String {
        return "BlackJackProcessModel(table=$table, editPlayer=$editPlayer, activePlayer=$activePlayer, activeHand=$activeHand, optionsAvailable=$optionsAvailable, optionSelected=$optionSelected"
    }
}
package es.rick.bpmn.blackjack.exception

import java.lang.Error

class BlackJackException(override val message: String) : Error(message) {
}
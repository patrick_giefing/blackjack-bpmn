package es.rick.bpmn.blackjack.spring

import camundajar.impl.scala.collection.immutable.TreeSeqMap
import es.rick.bpmn.blackjack.ProjectPackageClassPath
import es.rick.bpmn.blackjack.message.ConsoleMessagePublisher
import es.rick.bpmn.blackjack.message.MessagePublisher
import org.camunda.bpm.engine.ProcessEngine
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl
import org.camunda.bpm.engine.impl.cfg.SpringBeanFactoryProxyMap
import org.camunda.bpm.spring.boot.starter.configuration.Ordering
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.web.servlet.config.annotation.CorsRegistry

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import javax.annotation.PostConstruct


@SpringBootApplication(scanBasePackageClasses = [ProjectPackageClassPath::class])
open class BlackJackBpmnSpringBootStarter @Autowired constructor(
    private val applicationContext: ApplicationContext,
    private val processEngine: ProcessEngine
) {
    @PostConstruct
    fun setProcessEngineBeans() {
        val processEngineConfig =
            processEngine.processEngineConfiguration as ProcessEngineConfigurationImpl
        processEngineConfig.beans = SpringBeanFactoryProxyMap(applicationContext)
    }

    @Bean
    open fun messagePublisher(): MessagePublisher {
        return ConsoleMessagePublisher()
    }
}

@Configuration
@EnableWebMvc
open class WebConfig : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
    }
}

fun main(args: Array<String>) {
    runApplication<BlackJackBpmnSpringBootStarter>(*args)
}

package es.rick.bpmn.blackjack.spring

import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl
import org.camunda.bpm.engine.ProcessEngineConfiguration
import org.camunda.bpm.engine.ProcessEngine
import org.camunda.bpm.spring.boot.starter.configuration.Ordering
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(Ordering.DEFAULT_ORDER + 1)
class BlackJackProcessEnginePlugin : ProcessEnginePlugin {
    override fun preInit(processEngineConfiguration: ProcessEngineConfigurationImpl) {
        processEngineConfiguration.databaseSchemaUpdate = ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE
    }

    override fun postInit(processEngineConfiguration: ProcessEngineConfigurationImpl) {}
    override fun postProcessEngineBuild(processEngine: ProcessEngine) {}
}
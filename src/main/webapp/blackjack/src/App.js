import {
    BrowserRouter as Router,
    //Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import Game from "./components/Board/Board";
import Tables from "./components/Table/Tables";
import CreateTable from "./components/Table/CreateTable";
import Table from "./components/Table/Table";

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/game" component={Game} />
                <Route exact path="/tables/create" component={CreateTable} />
                <Route exact path="/tables" component={Tables} />
                <Route exact path="/table/:tableId" component={Table} />
            </Switch>
            <Redirect from="/" to="/tables"/>
        </Router>
    );
}

export default App;

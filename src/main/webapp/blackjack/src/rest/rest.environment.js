export const getEndoint = () => {
    if (window.location.hostname == 'localhost' || window.location.hostname == '127.0.0.1') {
        return 'http://127.0.0.1:8080/';
    }
    return '/';
}
import {createStore} from "redux";
import {ACTION_SET_ACTIVE_TABLE} from "./actions";

const initialState = {
    tables: null,
    activeTable: null
};

const rootReducer = (state = initialState, action) => {
    if (action.type == ACTION_SET_ACTIVE_TABLE) {
        return {
            ...state,
            activeTable: action.value
        };
    }
    console.info(`Unhandled state: ${state}, action: ${action}`);
    return state;
};

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(() => {
    console.info("callback fired");
});

//console.info(store.getState());
//store.dispatch({type: 'abc', value: {abc: 'def'}});

export default store;
import {ACTION_SET_ACTIVE_TABLE} from "./actions";

export class DispatchWrapper {
    constructor(dispatch) {
        this.dispatch = dispatch;
    }

    setStoreActiveTable = (table) => {
        this.dispatch({type: ACTION_SET_ACTIVE_TABLE, value: table});
    };
}
import React, {useState, useEffect} from 'react';
import "./Hand.css";
import PropTypes from "prop-types";
import DealerBox from "../Box/DealerBox";
import Card from "../Card/Card";

function getHandValue(cards) {
    let value = 0;
    let soft = false;
    let hard = false;
    cards?.forEach(card => {
        let cardValue = getCardValue(card.value);
        if (cardValue == 11) {
            if (value + cardValue > 21) {
                value += 1;
            } else {
                value += cardValue;
                soft = true;
            }
        } else {
            if (soft && value + cardValue > 21) {
                value += cardValue - 10;
                soft = false;
                hard = true;
            } else {
                value += cardValue;
            }
        }
    });
    return [value, (value > 21 ? 'Busted ' + value : (value == 21 && cards.length == 2 ? 'BlackJack' : (soft ? 'Soft ' : hard ? 'Hard ' : '') + value))];
}

function getCardValue(cardValue) {
    if (cardValue == 'A') return 11;
    if (cardValue == 'J' || cardValue == 'Q' || cardValue == 'K' || cardValue == '_10') return 10;
    return +cardValue.substring(1);
}

function Hand(props) {
    const [handValue, handValueString] = getHandValue(props.cards);

    if (!props.cards || props.cards.length == 0) return null;
    return (
        <div className="hand">
            <div className="cards">
                {props.cards?.map((card, index) =>
                    <div key={index} className={`card card${index + 1}`}><Card card={card}/></div>)}
                <div className={`value ${handValueString.toLowerCase()}`}>{handValueString}</div>
            </div>
        </div>
    );
}

Hand.propTypes = {
    cards: PropTypes.object
};

export default Hand;
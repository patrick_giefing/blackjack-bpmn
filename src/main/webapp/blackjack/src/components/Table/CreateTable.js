import React, {useState} from 'react';
import {Alert, AlertTitle} from '@material-ui/lab';
import {connect} from "react-redux";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import {Redirect} from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import {DispatchWrapper} from "../../redux/dispatch.wrappers";
import {Button, ButtonGroup, InputLabel} from "@material-ui/core";
import axios from "axios";
import {getEndoint} from '../../rest/rest.environment';

function CreateTable(props) {
    const [tableCreationCancelled, setTableCreationCancelled] = useState(false)
    const [modelCreated, setModelCreated] = useState(false)
    const [saveException, setSaveException] = useState(null)
    const [holeCard, setHoleCard] = useState(false)

    function saveTable() {
        const tableRules = {holeCardMode: holeCard ? 'HoleCard' : 'NoHoleCard'};
        axios.post(`${getEndoint()}table/create/`, tableRules).then(response => {
            setSaveException(null);
            setModelCreated(response.data);
        }).catch(reason => {
            setSaveException(reason.message)
            setModelCreated(null);
        });
    }

    if (tableCreationCancelled) {
        return <Redirect to="/tables"/>;
    }
    if (modelCreated) {
        return <Redirect to={`/table/${modelCreated.table.tableId}`}/>;
    }

    let errorMessage = null;
    if (saveException) {
        errorMessage = (<Alert severity="error">Error creating the table: {saveException}</Alert>);
    }

    return (
        <div style={{width: "300px"}}>
            {errorMessage}
            <FormControl fullWidth={true}>
                <InputLabel>Dealer hole card</InputLabel>
                <Checkbox
                    value={holeCard}
                    onChange={e => setHoleCard(e.target.checked)}
                />
            < /FormControl>
            <ButtonGroup fullWidth={true}>
                <Button onClick={saveTable}>Create table</Button>
                <Button onClick={() => setTableCreationCancelled(true)}>Cancel</Button>
            </ButtonGroup>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        //activeTable: state.activeTable
    };
};

const mapDispatchToProps = dispatch => {
    let dispatchWrapper = new DispatchWrapper(dispatch);
    return {
        createTable: (table) => dispatchWrapper.createTable(table)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTable);
import Board from "../Board/Board";
import React, {useState, useEffect} from "react";
import {Button, ButtonGroup} from "@material-ui/core";
import {Redirect} from "react-router-dom";
import EditPlayerDialog from "../Player/EditPlayerDialog";
import axios from 'axios';
import {getEndoint} from "../../rest/rest.environment";
import {Alert} from "@material-ui/lab";

function Table(props) {
    const tableId = props.match.params.tableId;
    const [backToTableList, setBackToTableList] = useState(false);
    const [showAddPlayer, setShowAddPlayer] = useState(false);
    const [players, setPlayers] = useState([
        {
            name: 'Adam', money: 10000, bet: 10, box: {
                hands: [
                    {
                        cards: [
                            {color: 'Clubs', value: 'A'},
                            {color: 'Spades', value: '_10'},
                            {color: 'Hearts', value: '_9'},
                            {color: 'Diamonds', value: 'J'}
                        ]
                    }
                ]
            }
        },
        {
            name: 'Bob', money: 10000, bet: 12, box: {
                hands: [
                    {
                        cards: [
                            {color: 'Clubs', value: '_8'},
                            {color: 'Spades', value: '_7'},
                            {color: 'Hearts', value: '_2'}
                        ]
                    }
                ]
            }
        },
        {
            name: 'Jack', money: 10000, bet: 15, box: {
                hands: [
                    {
                        cards: [
                            {color: 'Clubs', value: 'A'},
                            {color: 'Spades', value: 'A'},
                            {color: 'Hearts', value: '_4'}
                        ]
                    }
                ]
            }
        },
        {
            name: 'John', money: 10000, bet: 20, box: {
                hands: [
                    {
                        cards: [
                            {color: 'Diamonds', value: 'Q'},
                            {color: 'Diamonds', value: 'A'}
                        ]
                    }
                ]
            }
        },
        {
            name: 'Daisy', money: 15000, bet: 20, box: {
                hands: [
                    {
                        cards: [
                            {color: 'Spades', value: '_7'},
                            {color: 'Diamonds', value: '_8'},
                            {color: 'Spades', value: '_6'}
                        ]
                    }
                ]
            }
        }
    ]);
    const [dealer, setDealer] = useState({
        dealerId: '1',
        name: 'Jenkins Addams',
        box: {
            hand: {
                cards: [
                    {color: 'Clubs', value: 'A'},
                    {color: 'Hearts', value: '_9'}
                ]
            }
        }
    });
    const [model, setModel] = useState({});
    const [exception, setException] = useState(null);

    const setModelAndLog = (model) => {
        setModel(model);
        console.info("model", model)
    };

    useEffect(() => {
        axios.get(`${getEndoint()}table/${tableId}/`).then(response => {
            setModelAndLog(response.data);
        }).catch(reason => {
            setException(reason.message);
        });
    }, []);

    let editPlayerDialog = null;

    function savePlayer(player) {
        setShowAddPlayer(false);
        axios.post(`${getEndoint()}table/${tableId}/edit-player-and-execute/`, player, {params: {option: "addPlayer"}}).then(response => {
            setModelAndLog(response.data);
            setException(null);
        }).catch(reason => {
            setException(reason.message);
        });
        /*const playersExpanded = [...players, player]
        setPlayers(playersExpanded)
        console.info(playersExpanded);*/
    }

    function executeOption(option) {
        axios.post(`${getEndoint()}table/${tableId}/execute/`, null, {params: {option: option}}).then(response => {
            setModelAndLog(response.data);
            setException(null);
        }).catch(reason => {
            setException(reason.message);
        });
    }

    if (showAddPlayer) {
        editPlayerDialog = (
            <EditPlayerDialog player={{playerType: 'Human'}} save={(player) => savePlayer(player)}
                              cancel={() => setShowAddPlayer(false)}/>
        );
    }
    if (backToTableList) {
        return (<Redirect to="/tables/"/>);
    }
    let errorMessage = null;
    if (exception) {
        errorMessage = (<Alert severity="error">Error creating the table: {exception}</Alert>);
    }
    const gameButtons = [];
    const table = model?.table ?? {};
    const optionsAvailable = model?.optionsAvailable ?? [];
    if (optionsAvailable.includes("addPlayer")) {
        gameButtons.push(<Button onClick={() => setShowAddPlayer(true)}>Add player</Button>);
    }

    function createOptionButton(option, label) {
        return <Button className={option} onClick={() => executeOption(option)}>{label}</Button>;
    }

    if (optionsAvailable.includes("startGame")) {
        gameButtons.push(createOptionButton("startGame", "Start game"));
    }
    if (optionsAvailable.includes("hit")) {
        gameButtons.push(createOptionButton("hit", "Hit"));
    }
    if (optionsAvailable.includes("stay")) {
        gameButtons.push(createOptionButton("stay", "Stay"));
    }
    if (optionsAvailable.includes("split")) {
        gameButtons.push(createOptionButton("split", "Split"));
    }
    if (optionsAvailable.includes("doubleDown")) {
        gameButtons.push(createOptionButton("doubleDown", "Double down"));
    }
    if (optionsAvailable.includes("play")) {
        gameButtons.push(createOptionButton("play", "Play"));
    }
    if (optionsAvailable.includes("surrender")) {
        gameButtons.push(createOptionButton("surrender", "Surrender"));
    }
    if (optionsAvailable.includes("insurance")) {
        gameButtons.push(createOptionButton("insurance", "Insurance"));
    }
    if (optionsAvailable.includes("noInsurance")) {
        gameButtons.push(createOptionButton("noInsurance", "No insurance"));
    }
    return (
        <div>
            {errorMessage}
            {editPlayerDialog}
            <Board players={table.players} dealer={table.dealer}/>
            <ButtonGroup>{gameButtons}</ButtonGroup>
            <br/>
            <ButtonGroup>
                <Button onClick={() => setBackToTableList(true)}>Back to table list</Button>
            </ButtonGroup>
        </div>
    );
}

export default Table;
import React, {useState, useEffect} from 'react';
import {DataGrid} from '@material-ui/data-grid';
//import {connect} from "react-redux";
//import {DispatchWrapper} from "../redux/dispatch.wrappers";
import {Redirect} from "react-router-dom";
import {Button, ButtonGroup} from "@material-ui/core";
import axios from "axios";
import {getEndoint} from "../../rest/rest.environment";
import {Alert} from "@material-ui/lab";

function Tables(props) {
    const [activeTable, setActiveTable] = useState(null);
    const [createNewTable, setCreateNewTable] = useState(false);
    const [models, setModels] = useState([]);
    const [exception, setException] = useState(null);

    const handleRowSelection = function (row) {
        let table = row.data.table;
        setActiveTable(table);
    }

    useEffect(() => {
        axios.get(`${getEndoint()}/tables/`).then(response => {
            setException(null);
            setModels(response.data);
        }).catch(reason => {
            setException(reason.message);
            setModels(null);
        });
    }, []);

    // https://codesandbox.io/s/3u8si?file=/demo.js:84-646
    const columns = [
        {field: 'tableId', headerName: 'ID', width: 320, valueGetter: params => params.row.table.tableId},
        {
            field: 'tableRules',
            headerName: 'Rules',
            width: 130,
            valueGetter: params => params.row.table.tableRules?.holeCardMode
        },
        {field: 'dealer.name', headerName: 'Dealer', width: 160, valueGetter: params => params.row.table.dealer?.name},
        {
            field: 'players',
            headerName: 'Players',
            width: 130,
            type: 'number',
            valueGetter: params => params.row.table.players?.length
        },
        {
            field: 'activeGame', headerName: 'Status', width: 300, valueGetter: params => {
                let activeGame = params.row.table.activeGame;
                if (!activeGame) return 'Waiting for game to start';
                return 'Game in progress';
            }
        }
    ];
    let errorMessage = null;
    if (exception) {
        errorMessage = (<Alert severity="error">Error creating the table: {exception}</Alert>);
    }

    if (createNewTable) {
        return (<Redirect to="/tables/create"/>);
    } else if (activeTable)
        return (<Redirect to={`/table/${activeTable.tableId}/`}/>);
    else
        return (
            <div style={{height: 400, width: '100%'}}>
                {errorMessage}
                <DataGrid
                    rows={models}
                    columns={columns}
                    pageSize={20}
                    getRowId={row => row.table.tableId}
                    onRowSelected={handleRowSelection}
                />
                <ButtonGroup fullWidth={true}>
                    <Button onClick={() => setCreateNewTable(true)}>Create new table</Button>
                </ButtonGroup>
            </div>
        );
}

export default Tables;

// Redux Test
/*const mapStateToProps = state => {
    return {
        activeTable: state.activeTable
    };
};

const mapDispatchToProps = dispatch => {
    let dispatchWrapper = new DispatchWrapper(dispatch);
    return {
        setActiveTable: (table) => dispatchWrapper.setStoreActiveTable(table)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tables);*/
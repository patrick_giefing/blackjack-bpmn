import table from '../../img/blackjack-table.svg'
import {Redirect} from "react-router-dom";
import React, {useState} from "react";
import "./Board.css";
import PropTypes from "prop-types";
import PlayerBox from "../Box/PlayerBox";
import EditPlayerDialog from "../Player/EditPlayerDialog";
import DealerBox from "../Box/DealerBox";

function Board(props) {
    return (
        <div className="GameTable">
            <img style={{width: "1600px"}} src={table}/>
            <div className="Players">
                {props.players?.map((player, index) => (
                    <div key={index} className={`box box${index + 1}`}>
                        <PlayerBox player={player}/>
                    </div>))}
            </div>
            <div className="Dealer">
                <DealerBox dealer={props.dealer}/>
            </div>
        </div>
    );
}

Board.propTypes = {
    players: PropTypes.array,
    dealer: PropTypes.object
};

export default Board;

import PropTypes from "prop-types";
import Game from "./DealerBox.css";
import Hand from "../Hand/Hand";

function DealerBox(props) {
    return (
        <div className="box">
            <div className="dealerInfo">
                <div className="dealerName">{props.dealer?.name}</div>
            </div>
            <Hand cards={props.dealer?.box?.hand?.cards}/>
        </div>
    );
}

DealerBox.propTypes = {
    dealer: PropTypes.object
};

export default DealerBox;
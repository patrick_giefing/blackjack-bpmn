import PropTypes from "prop-types";
import BoxStyles from "./PlayerBox.css";
import Hand from "../Hand/Hand";

function PlayerBox(props) {
    return (
        <div style={BoxStyles} className="box">
            <div className="playerInfo">
                <div className="playerName">{props.player.name}</div>
                <div className="balance">
                    Balance: <div className="amount">{props.player.balance}</div>
                </div>
                <div className="bet">
                    Bet: <div className="amount">{props.player.bet}</div>
                </div>
            </div>
            <div className="hands">
                {props.player.box?.hands?.map((hand, index) =>
                    <div key={index} className={`hand hand${index + 1}`}><Hand cards={hand.cards}/></div>)}
            </div>
        </div>);
}

PlayerBox.propTypes = {
    player: PropTypes.object.isRequired
};

export default PlayerBox;
import './Card.css';
import PropTypes from "prop-types";
import DealerBox from "../Box/DealerBox";

function Card(props) {
    function translateCardValueToCssClass(s) {
        if (s == 'J') return 'jack';
        if (s == 'Q') return 'queen';
        if (s == 'K') return 'king';
        if (s == 'A') return 'ace';
        return s;
    }

    const classNameColor = props.card.color?.toLowerCase();
    const classNameValue = translateCardValueToCssClass(props.card.value);
    return (<div className={`card ${classNameColor} ${classNameValue}`}></div>);
}

Card.propTypes = {
    card: PropTypes.object.isRequired
};

export default Card;
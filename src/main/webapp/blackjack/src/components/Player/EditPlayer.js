import {Checkbox, FormControl, FormControlLabel, InputLabel, TextField} from "@material-ui/core";
import PropTypes from 'prop-types';

function EditPlayer(props) {
    //playerId: String, val name: String, val playerType: PlayerType, val money: Int, val bet: Int
    return (
        <div>
            <FormControl>
                <TextField label={"Name"} value={props.player.name}
                           onChange={event => props.player.name = event.target.value}/>
            </FormControl>
            <br/>
            <FormControl>
                <TextField label={"Money"} type={"number"} value={props.player.balance}
                           onChange={event => props.player.balance = +event.target.value}/>
            </FormControl>
            <br/>
            <FormControl>
                <TextField label={"Bet"} type={"number"} value={props.player.bet}
                           onChange={event => props.player.bet = +event.target.value}/>
            </FormControl>
        </div>
    );
}

EditPlayer.propTypes = {
    player: PropTypes.object.isRequired
};

export default EditPlayer;
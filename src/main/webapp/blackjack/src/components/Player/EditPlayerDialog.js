import EditPlayer from "./EditPlayer";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@material-ui/core";
import PropTypes from "prop-types";

function EditPlayerDialog(props) {
    // https://material-ui.com/components/dialogs/
    return (
        <Dialog open={true}>
            <DialogTitle>Edit player</DialogTitle>
            <DialogContent>
                <EditPlayer player={props.player}/>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.save(props.player)}>Save player</Button>
                <Button onClick={props.cancel}>Cancel</Button>
            </DialogActions>
        </Dialog>
    );
}

EditPlayerDialog.propTypes = {
    player: PropTypes.object.isRequired,
    cancel: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired
};

export default EditPlayerDialog;
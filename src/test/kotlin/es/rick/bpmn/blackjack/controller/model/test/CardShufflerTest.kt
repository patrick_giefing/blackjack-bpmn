package es.rick.bpmn.blackjack.controller.model.test

import es.rick.bpmn.blackjack.controller.CardShuffler
import org.junit.Assert.*
import org.junit.jupiter.api.Test

class CardShufflerTest {
    @Test
    fun shuffle() {
        val shuffler = CardShuffler()
        val deckCount = 4
        val cardsShuffeld1 = shuffler.shuffle(deckCount)
        val cardsShuffeld2 = shuffler.shuffle(deckCount)
        val deckSize = 52
        assertEquals(deckSize * deckCount, cardsShuffeld1.cards.size)
        assertTrue(cardsShuffeld1.cards.size == cardsShuffeld2.cards.size)
        assertTrue(cardsShuffeld1 != cardsShuffeld2)
    }
}
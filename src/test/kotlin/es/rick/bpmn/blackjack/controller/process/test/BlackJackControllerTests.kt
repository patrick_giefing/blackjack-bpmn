package es.rick.bpmn.blackjack.controller.process.test

import com.fasterxml.jackson.databind.ObjectMapper
import es.rick.bpmn.blackjack.controller.DealerController
import es.rick.bpmn.blackjack.controller.TableController
import es.rick.bpmn.blackjack.message.ConsoleMessagePublisher
import es.rick.bpmn.blackjack.message.MessagePublisher
import es.rick.bpmn.blackjack.model.*
import es.rick.bpmn.blackjack.spring.BlackJackBpmnSpringBootStarter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Controller

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [BlackJackBpmnSpringBootStarter::class, TableControllerTests.TestConfig::class]
)
class TableControllerTests(
    @Autowired val tableController: TableController,
    @Autowired val messagePublisher: ListMessagePublisher
) {
    val consoleMessagePublisher = ConsoleMessagePublisher()

    open class TestConfig {
        @Bean
        @Primary
        open fun listMessagePublisher(): ListMessagePublisher {
            return ListMessagePublisher()
        }
    }

    @Test
    fun createAndCloseTables() {
        val tableRules = TableRules()
        var model = tableController.createTable(tableRules)
        assertEquals(1, model.optionsAvailable.size)
        val tableId = model.table.tableId

        messagePublisher.clearMessages()
        // Try to start game without players
        assertEquals(0, messagePublisher.getMessageCount())
        tableController.executeOption(tableId, "startGame")
        assertEquals(1, messagePublisher.getMessageCount())
        messagePublisher.clearMessages()

        var player1 =
            Player(null, "Player 1", PlayerType.Human, 1000, 10, PlayerBox(mutableListOf(), PlayerBoxStatus.Inactive))
        var player2 =
            Player(null, "Player 2", PlayerType.Human, 1000, 10, PlayerBox(mutableListOf(), PlayerBoxStatus.Inactive))
        var player3 =
            Player(null, "Player 3", PlayerType.Human, 1000, 10, PlayerBox(mutableListOf(), PlayerBoxStatus.Inactive))
        tableController.setEditPlayerAndExecuteOption(tableId, player1, "addPlayer")
        tableController.setEditPlayerAndExecuteOption(tableId, player2, "addPlayer")
        tableController.setEditPlayerAndExecuteOption(tableId, player3, "addPlayer")

        var models = tableController.getTables()
        assertEquals(1, models.size)
        assertEquals(3, models[0].table.players.size)
        assertEquals(3, models[0].optionsAvailable?.size)

        val players = models[0].table.players
        player1 = players[0]
        player2 = players[1]
        player3 = players[2]

        assertEquals(0, messagePublisher.getMessageCount())

        tableController.setEditPlayerAndExecuteOption(tableId, player1, "removePlayer")
        models = tableController.getTables()
        assertEquals(1, models.size)
        assertEquals(2, models[0].table.players.size)

        tableController.setEditPlayerAndExecuteOption(tableId, player3, "removePlayer")
        models = tableController.getTables()
        assertEquals(1, models.size)
        assertEquals(1, models[0].table.players.size)

        // try to remove player 1 again - not possible
        assertEquals(0, messagePublisher.getMessageCount())
        tableController.setEditPlayerAndExecuteOption(tableId, player1, "removePlayer")
        models = tableController.getTables()
        assertEquals(1, models.size)
        assertEquals(1, models[0].table.players.size)
        assertEquals(1, messagePublisher.getMessageCount())
        assertEquals(3, models[0].optionsAvailable?.size)

        tableController.executeOption(tableId, "startGame")
        models = tableController.getTables()
        //prettyPrintJson(models)
        assertTrue(models[0].optionsAvailable?.size >= 3)

        model = tableController.getTable(tableId)
        while (model.optionsAvailable.contains("hit")) {
            consoleMessagePublisher.sendToAll("hit")
            tableController.executeOption(tableId, "hit");
            model = tableController.getTable(tableId)
            println("Hand value: ${model.activeHand?.getHandValue()}")
            //prettyPrintJson(model)
        }
    }

    private fun prettyPrintJson(o: Any) {
        val mapper = ObjectMapper()
        val s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o)
        consoleMessagePublisher.sendToAll(s)
    }
}

class DealerControllerTests {
    val dealerController = DealerController()

    @Test
    fun findAvailableDealers() {
        val dealers = setOf(
            dealerController.findAvailableDealer(),
            dealerController.findAvailableDealer(),
            dealerController.findAvailableDealer()
        )
        assertEquals(3, dealers.size)
    }
}

class ListMessagePublisher : MessagePublisher {
    val consoleMessagePublisher = ConsoleMessagePublisher()
    val messages = mutableListOf<String>()

    override fun sendToAll(message: String) {
        messages.add(message)
        consoleMessagePublisher.sendToAll(message)
    }

    fun removeAndGetLastMessage(): String? {
        if (messages.isEmpty()) return null
        return messages.removeLast()
    }

    fun getMessageCount(): Int {
        return messages.size
    }

    fun clearMessages() {
        messages.clear()
        consoleMessagePublisher.frameMessage("Cleared all messages", 'i')
    }
}